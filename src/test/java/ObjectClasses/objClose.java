package ObjectClasses;

import org.openqa.selenium.WebDriver;

public class objClose {
	static WebDriver driver;
	
	public objClose(WebDriver driver) {
		objClose.driver = driver;
	}
	
	public static void closeDriver() {
		driver.close();
	}
	
	public static void quitDriver() {
		driver.quit();
	}
}
