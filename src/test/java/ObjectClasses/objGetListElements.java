package ObjectClasses;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class objGetListElements {
	static WebDriver driver;
	
	public objGetListElements(WebDriver driver) {
		objGetListElements.driver = driver;
	}
	
	public static void listElements() {
		
		List<WebElement> a = driver.findElements(By.tagName("a"));
		
		for( WebElement i : a) {
			System.out.println(i.getText());
			i.click();
			//i.getScreenshotAs(OutputType.FILE);
		}
	}
}
