package ObjectClasses;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import support.automate_helper;

public class objWebsite {
	static WebDriver driver;
	
	public objWebsite(WebDriver driver) {
		objWebsite.driver = driver;
		
	}
	
	public static void getUrl(String url) throws IOException {
		new automate_helper(driver);
		driver.get(automate_helper.property().getProperty(url));
		
	}
}
